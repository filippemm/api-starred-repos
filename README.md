# ApiStarredRepos

This is a project that adds tag functionality to its github starred repositories using the [GitHub API](https://docs.github.com/pt/rest).

## Front-End Project

[AppStarredRepos](https://gitlab.com/filippemm/app-starred-repos)

## Published Website

If you wish to view this project without running it locally, it is online at [https://mystarredrepos.filippemafra.dev/](https://mystarredrepos.filippemafra.dev/). This is a production version running with [Nginx](https://www.nginx.com), SSL with [Let's Encrypt](https://letsencrypt.org/) and [Certbot](https://certbot.eff.org), on a [DigitalOcean](https://m.do.co/c/8c53e4a98a57) droplet. The CI/CD was make with [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

## System Environment

- [Git](https://git-scm.com) 2.30.1 (or higher);
- [Node](https://nodejs.org/) 14.17.3 (or higher).
- [Nodemon](https://www.npmjs.com/package/nodemon) 2.0.12 (or higher) globally installed.
- [MongoAtlas Cloud](https://www.mongodb.com/en/cloud/atlas) (or [MongoDB Community](https://www.mongodb.com/try/download/community) locally) version ^5

## Development server

### Run development server

1. Install dependencies with `npm install`.

2. Create `nodemon.json` file and enter your `DB_URL`:

```
{
  "env": {
    "NODE_ENV": "development",
    "DB_URL": "mongodb://user:password/database"
  }
}
```

3. Run `npm run watch` to watch '.ts' files and transpile in '.js' files

4. Run nodemon in another terminal:

```
nodemon ./dist/main.js --config ./nodemon.json
```

### Run tests

Be careful, all data from this database will be deleted in this test. Tests were done with [Jest](https://jestjs.io).

1. Make sure you have everything up to date with `npm install`.

2. Configure your test database on `jest.startup.ts` file and modify 'mongodb://localhost/api-test-db'.

3. Run `npm test` for a component tests.

## Future plans

- Receive in one call all repositories from [AppStarredRepos](https://gitlab.com/filippemm/app-starred-repos);
- Possibility to delete repositories when synchronizing repositories;
- Improve mobile experience;
- Signin and login with Github.
