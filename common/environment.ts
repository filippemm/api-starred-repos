export const environment = {
  client: { name: process.env.CLIENT_NAME || 'Localhost Test' },
  server: { port: process.env.SERVER_PORT || 3000 },
  db: { url: process.env.DB_URL || 'mongodb://localhost/meat-api' },
  security: {
    saltRounds: process.env.SALT_ROUNDS || 10,
    apiSecret: process.env.API_SECRET || 'localhost%-20mac21',
    enableHTTPS: process.env.ENABLE_HTTPS || false,
    certificate: process.env.CERT_FILE || './security/keys/key.pem',
    key: process.env.CERT_KEY_FILE || './security/keys/cert.pem',
  },
  log: {
    level: process.env.LOG_LEVEL || 'debug',
    name: 'api-logger',
  },
};
