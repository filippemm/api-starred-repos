import { Document, Model, DocumentQuery, Query, EnforceDocument, Types, FilterQuery } from 'mongoose';
import { Router } from './router';
import { NotFoundError, BadRequestError } from 'restify-errors';
import { Request } from 'restify';

export abstract class ModelRouter<D extends Document> extends Router {
  basePath: string;
  pageSize: number = 10;

  orders = { id: { _id: 'desc' } };
  mongoQuery: FilterQuery<D> = {};

  constructor(protected model: Model<D>) {
    super();
    this.basePath = `/${this.model.collection.name}`;
  }

  order(order = 'id') {
    let response = this.orders[order];
    if (!response) {
      return this.orders['id'];
    }
    return response;
  }

  envelope(document: any): any {
    let resource = Object.assign({ _links: {} }, document.toJSON());
    resource._links.self = `${this.basePath}/${resource._id}`;
    return resource;
  }

  envelopeAll(documents: any[], options: any = {}): any {
    const resource: any = {
      _links: {
        self: `${options.url}`,
      },
      _information: {
        count: options.count,
        pageSize: options.pageSize,
      },
      items: documents,
    };

    if (options.page && options.count && options.pageSize) {
      let otherParams = options.otherParams ? `${options.otherParams}&` : '';
      if (options.page > 1) {
        resource._links.previous = `${this.basePath}?${otherParams}_page=${options.page - 1}`;
      }
      const remaining = options.count - options.page * options.pageSize;
      if (remaining > 0) {
        resource._links.next = `${this.basePath}?${otherParams}_page=${options.page + 1}`;
      }
    }
    return resource;
  }

  validateId = (req, res, next) => {
    if (!Types.ObjectId.isValid(req.params.id)) {
      next(new NotFoundError('Register not found'));
    } else {
      next();
    }
  };

  protected prepareOne(
    query: Query<EnforceDocument<D, {}>, EnforceDocument<D, {}>, {}, D>
  ): Query<EnforceDocument<D, {}>, EnforceDocument<D, {}>, {}, D> {
    return query;
  }

  protected prepareAll(
    query: Query<EnforceDocument<D, {}>[], EnforceDocument<D, {}>, {}, D>
  ): Query<EnforceDocument<D, {}>[], EnforceDocument<D, {}>, {}, D> {
    return query;
  }

  findAll = (req, res, next) => {
    let page = parseInt(req.query._page || 1);
    page = page > 0 ? page : 1;

    let pageSize = this.pageSize;
    let skip = (page - 1) * pageSize;

    let mongoQuery = this.mongoQuery;

    if (req.query._opt == 'all') {
      skip = 0;
      pageSize = 0;
    }
    const documentOrder = this.order(req.query._order);

    // this.model.find();

    this.model
      .count(mongoQuery)
      .exec()
      .then((count) =>
        this.prepareAll(this.model.find(mongoQuery).skip(skip).limit(pageSize).sort(documentOrder)).then(
          this.renderAll(res, next, {
            page,
            count,
            pageSize: pageSize ? pageSize : count,
            url: req.url,
          })
        )
      )
      .catch(next);
  };

  findById = (req, res, next) => {
    this.prepareOne(this.model.findById(req.params.id)).then(this.render(res, next)).catch(next);
  };

  save = (req, res, next) => {
    let document = new this.model(req.body);
    const bSave = this.beforeSave(document, req);
    if (bSave.can) {
      document.save().then(this.render(res, next)).catch(next);
    } else {
      next(new BadRequestError(bSave.message));
    }
  };

  beforeSave = (document: any, req: any): { can: boolean; message: string } => {
    return { can: true, message: '' };
  };

  replace = (req: Request, res, next) => {
    const options = {
      runValidators: true,
      overwrite: true,
    };
    this.model
      .update({ _id: req.params.id }, req.body, options)
      .exec()
      .then((result) => {
        if (result.n) {
          return this.prepareOne(this.model.findById(req.params.id)).exec();
        } else {
          throw new NotFoundError('Register not found');
        }
      })
      .then(this.render(res, next))
      .catch(next);
  };

  update = (req, res, next) => {
    const options = {
      runValidators: true,
      new: true, // Forces get new record
    };
    let document = new this.model(req.body);
    const bUpdate = this.beforeUpdate(document, req);
    if (bUpdate.can) {
      this.model.findByIdAndUpdate(req.params.id, req.body, options).then(this.render(res, next)).catch(next);
    } else {
      next(new BadRequestError(bUpdate.message));
    }
  };

  beforeUpdate = (document: any, req: any): { can: boolean; message: string } => {
    return { can: true, message: '' };
  };

  delete = (req, res, next) => {
    const options = {
      runValidators: false,
      new: true, // Força a pegar o novo registro
    };

    this.model
      .remove({ _id: req.params.id })
      .exec()
      .then((cmdResult: any) => {
        if (cmdResult.n) {
          res.send(204);
        } else {
          throw new NotFoundError('Register not found');
        }
        return next();
      })
      .catch(next);
  };
}
