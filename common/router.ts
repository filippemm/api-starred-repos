import { Server, Response, Next } from 'restify';
import { EventEmitter } from 'events';
import { NotFoundError } from 'restify-errors';

export abstract class Router extends EventEmitter {
  abstract applyRoutes(application: Server);

  envelope(document: any): any {
    return document;
  }

  envelopeAll(documents: any[], options: any = {}): any {
    return documents;
  }

  render(response: Response, next: Next) {
    return (document) => {
      if (document) {
        this.emit('beforeRender', document);
        response.json(this.envelope(document));
      } else {
        throw new NotFoundError('Register not found');
      }
      return next(false);
    };
  }

  doNotRender(response: Response, next: Next) {
    return (document) => {
      if (document) {
        response.send(204);
        //response.json({_id: document._id});
      } else {
        throw new NotFoundError('Register not found');
      }
      return next(false);
    };
  }

  renderAll(response: Response, next: Next, options: any = {}) {
    return (documents: any[]) => {
      if (documents) {
        documents.forEach((document, index, array) => {
          this.emit('beforeRender', document);
          array[index] = this.envelope(document);
        });
        response.json(this.envelopeAll(documents, options));
      } else {
        response.json(this.envelopeAll([]));
      }
      return next(false);
    };
  }

  passesRouteOptions = (req, res, next) => {
    res.send(204);
    return next();
  };
}
