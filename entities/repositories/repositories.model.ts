import { Document, Model, model, Schema, Types } from 'mongoose';
import { User } from '../users/users.model';

export interface Repository extends Document {
  user: Types.ObjectId | User;
  id: string;
  name: string;
  description: string;
  url: string;
  tags: string[];
  hasTag(tag: string): boolean;
}

export interface RepositoryModule extends Model<Repository> {
  findByTag(tag: string, projection?: string): Promise<Repository>;
}

const repositorySchema = new Schema<Repository>({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  id: {
    type: String,
    required: false,
  },
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: false,
  },
  url: {
    type: String,
    required: false,
  },
  tags: {
    type: [String],
    required: false,
  },
});

repositorySchema.statics.findLikeTag = function (search: string, user_id: string, projection: string) {
  return this.find({ tags: new RegExp(`.*${search}.*`, 'i'), user: user_id }, projection);
  // return this.find({ tags: { $contains: search } }, projection);
};

repositorySchema.statics.findByUser_id = function (user_id: string, projection: string) {
  return this.find({ user: user_id }, projection);
};

repositorySchema.methods.hasTag = function (tagSearch: string): boolean {
  return this.tags.some((tag) => tag.includes(tagSearch));
};

export const Repository = model<Repository, RepositoryModule>('Repository', repositorySchema);
