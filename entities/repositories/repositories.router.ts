import { Server } from 'restify';
import { ModelRouter } from '../../common/model-router';
import { Repository } from './repositories.model';
import { authorize } from '../../security/authz.handler';
import { BadRequestError } from 'restify-errors';
import { EnforceDocument, Query } from 'mongoose';

class RepositoriesRouter extends ModelRouter<Repository> {
  constructor() {
    super(Repository);
    this.on('beforeRender', (document) => {
      document.user = undefined;
    });
  }

  saveRepository = (req, res, next) => {
    // Default profile
    req.body.user = req.authenticated._id;
    this.save(req, res, next);
  };

  findAllMy = (req, res, next) => {
    let page = parseInt(req.query._page || 1);
    page = page > 0 ? page : 1;
    let pageSize = this.pageSize;
    let skip = (page - 1) * pageSize;
    let mongoQuery = { user: req.authenticated._id };

    if (req.query._opt == 'all') {
      skip = 0;
      pageSize = 0;
    }

    const documentOrder = this.order(req.query._order);

    this.model
      .count(mongoQuery)
      .exec()
      .then((count) =>
        this.prepareAll(this.model.find(mongoQuery).skip(skip).limit(pageSize).sort(documentOrder)).then(
          this.renderAll(res, next, {
            page,
            count,
            pageSize: pageSize ? pageSize : count,
            url: req.url,
          })
        )
      )
      .catch(next);
  };

  findAllMyByTag = (req, res, next) => {
    const search = req.query._search;
    if (search) {
      let page = parseInt(req.query._page || 1);
      page = page > 0 ? page : 1;
      let pageSize = this.pageSize;
      let skip = (page - 1) * pageSize;
      let mongoQuery = { tags: new RegExp(`.*${search}.*`, 'i'), user: req.authenticated._id };

      if (req.query._opt == 'all') {
        skip = 0;
        pageSize = 0;
      }

      const documentOrder = this.order(req.query._order);

      this.model
        .count(mongoQuery)
        .exec()
        .then((count) =>
          this.prepareAll(this.model.find(mongoQuery).skip(skip).limit(pageSize).sort(documentOrder)).then(
            this.renderAll(res, next, {
              page,
              count,
              pageSize: pageSize ? pageSize : count,
              url: req.url,
            })
          )
        )
        .catch(next);
    } else {
      next();
    }
  };

  isDistinct = async (req, res, next) => {
    const { id } = req.body;
    const authUser = req.authenticated;

    const found = await this.model.count({ user: authUser._id, id });

    if (found) {
      next(new BadRequestError('This repository has already been registered.'));
    } else {
      next();
    }
  };

  updateTags = (req, res, next) => {
    req.body = { tags: req.body.tags };
    this.update(req, res, next);
  };

  isOwner = async (req, res, next) => {
    const { id } = req.body;
    const authUser = req.authenticated;

    const found = await this.model.count({ user: authUser._id, id });

    if (!found) {
      next(new BadRequestError('This repository is not yours.'));
    } else {
      next();
    }
  };

  protected prepareAll(
    query: Query<EnforceDocument<Repository, {}>[], EnforceDocument<Repository, {}>, {}, Repository>
  ): Query<EnforceDocument<Repository, {}>[], EnforceDocument<Repository, {}>, {}, Repository> {
    return query.populate('user', 'user');
  }

  applyRoutes(application: Server) {
    application.get(`${this.basePath}`, [authorize('admin', 'user'), this.findAllMyByTag, this.findAllMy]);
    application.post(`${this.basePath}`, [authorize('admin', 'user'), this.isDistinct, this.saveRepository]);
    application.patch(`${this.basePath}/:id`, [
      authorize('admin', 'user'),
      this.validateId,
      this.isOwner,
      this.updateTags,
    ]);

    application.del(`${this.basePath}/:id`, [authorize('admin', 'user'), this.validateId, this.isOwner, this.delete]);
  }
}

export const repositoriesRouter = new RepositoriesRouter();
