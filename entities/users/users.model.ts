import { Document, Model, model, Schema } from 'mongoose';
import { compareSync, hash } from 'bcrypt';

import { environment } from '../../common/environment';

export interface User extends Document {
  name: string;
  email: string;
  password: string;
  profiles: string[];
  githubUsername: string;
  matches(password: string): boolean;
  hasAny(...profiles: string[]): boolean;
}

export interface UserModule extends Model<User> {
  findByEmail(email: string, projection?: string): Promise<User>;
}

const userSchema = new Schema<User>({
  name: {
    type: String,
    required: true,
    maxlength: 80,
    minlength: 3,
  },
  email: {
    type: String,
    unique: true,
    validate: {
      validator: function (v) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          v
        );
      },
      // TODO (i18n) return 'invalid.email' instead
      message: 'Path `{PATH}` (`{VALUE}`) is invalid email.',
    },
    required: true,
  },
  password: {
    type: String,
    select: false,
    required: true,
    minlength: 6,
  },
  githubUsername: {
    type: String,
    required: true,
  },
  profiles: {
    type: [String],
    required: false,
  },
});

userSchema.statics.findByEmail = function (email: string, projection: string) {
  return this.findOne({ email }, projection);
};

userSchema.methods.matches = function (password: string): boolean {
  return compareSync(password, this.password);
};

userSchema.methods.hasAny = function (...profiles: string[]): boolean {
  return profiles.some((profile) => this.profiles.indexOf(profile) !== -1);
};

const hashPassword = (obj, next) => {
  hash(obj.password, environment.security.saltRounds)
    .then((hash) => {
      obj.password = hash;
      next();
    })
    .catch(next);
};

const saveMiddleware = function (next) {
  const user: User = this;
  if (!user.isModified('password')) {
    next();
  } else {
    hashPassword(user, next);
  }
};

const updateMiddleware = function (next) {
  if (!this.getUpdate().password) {
    next();
  } else {
    hashPassword(this.getUpdate(), next);
  }
};

// midleware 'this' representa o documento em questão
// NÃO USAR ARROW FUNCTION

userSchema.pre('save', saveMiddleware);
userSchema.pre('findOneAndUpdate', updateMiddleware);
userSchema.pre('update', updateMiddleware);

export const User = model<User, UserModule>('User', userSchema);
