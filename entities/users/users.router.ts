import * as restify from 'restify';

import { ModelRouter } from '../../common/model-router';
import { User } from './users.model';
import { authenticate } from '../../security/auth.handler';
import { authorize } from '../../security/authz.handler';

class UsersRouter extends ModelRouter<User> {
  constructor() {
    super(User);
    this.on('beforeRender', (document) => {
      document.password = undefined;
    });
    this.mongoQuery = {
      profiles: { $not: /^admin$/ }, // TODO if logged user is not a 'administrator'
    };
  }

  saveUser = (req, res, next) => {
    // Default profile
    req.body.profiles = ['user'];
    this.save(req, res, next);
  };

  findByEmail = (req, res, next) => {
    if (req.query.email) {
      User.findByEmail(req.query.email)
        .then((user) => (user ? [user] : []))
        .then(
          this.renderAll(res, next, {
            pageSize: this.pageSize,
            url: req.url,
          })
        )
        .catch(next);
    } else {
      next();
    }
  };

  authenticated = (req, res, next) => {
    res.send(204);
    return next();
  };

  applyRoutes(application: restify.Server) {
    application.get({ path: `${this.basePath}`, version: '2.0.0' }, [
      authorize('admin'),
      this.findByEmail,
      this.findAll,
    ]);

    application.get(`${this.basePath}/:id`, [authorize('admin'), this.validateId, this.findById]);
    application.post(`${this.basePath}`, [this.saveUser]);
    application.put(`${this.basePath}/:id`, [authorize('admin'), this.validateId, this.replace]);
    application.patch(`${this.basePath}/:id`, [authorize('admin'), this.validateId, this.update]);
    application.del(`${this.basePath}/:id`, [authorize('admin'), this.validateId, this.delete]);

    application.opts(`${this.basePath}/:id`, [this.passesRouteOptions]);
    application.opts(`${this.basePath}`, [authorize('admin'), this.passesRouteOptions]);

    application.post(`${this.basePath}/authenticate`, authenticate);
    application.get(`${this.basePath}/auth/authenticated`, [authorize('admin, user'), this.authenticated]);

    application.opts(`${this.basePath}/authenticate`, this.passesRouteOptions);
    application.opts(`${this.basePath}/authenticated`, this.passesRouteOptions);
  }
}

export const usersRouter = new UsersRouter();
