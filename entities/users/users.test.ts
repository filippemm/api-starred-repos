import 'jest';
import * as request from 'supertest';

let address: string = (<any>global).address;
let auth: string = (<any>global).auth;

test('get /users', () => {
  return request(address)
    .get('/users')
    .set('Authorization', auth)
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
    })
    .catch((error) => {
      throw error;
    });
});

test('post /users', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      name: 'Some Random Hero',
      githubUsername: 'somegithubuser',
      email: 'noreply@dc.com',
      password: '123456', //minimum 6
    })
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body._id).toBeDefined();
      expect(response.body.profiles[0]).toBe('user');
      expect(response.body.name).toBe('Some Random Hero');
      expect(response.body.githubUsername).toBe('somegithubuser');
      expect(response.body.email).toBe('noreply@dc.com');
      expect(response.body.password).toBeUndefined();
    })
    .catch((error) => {
      throw error;
    });
});

// TODO Cannot assign to read only property 'toJSON' of object 'ValidationError:...'
test('post /users - nome obrigatorio', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      email: 'user12@gmail.com',
      password: '123456',
    })
    .then((response) => {
      expect(response.status).toBe(400);
      expect(response.body.message).toContain('`name` is required');
    })
    .catch((error) => {
      throw error;
    });
});

test('post /users - email duplicado', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      name: 'dupe',
      githubUsername: 'somegithubuser',
      email: 'dupe@gmail.com',
      password: '123456',
    })
    .then((response) =>
      request(address).post('/users').set('Authorization', auth).send({
        name: 'dupe',
        githubUsername: 'somegithubuser',
        email: 'dupe@gmail.com',
        password: '123456',
      })
    )
    .then((response) => {
      expect(response.status).toBe(400);
      expect(response.body.message).toContain('E11000 duplicate key');
    })
    .catch((error) => {
      throw error;
    });
});

test('get /users - findByEmail', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      name: 'usuario 3',
      githubUsername: 'somegithubuser',
      email: 'usuario3@email.com',
      password: '123456',
    })
    .then((response) =>
      request(address).get('/users').set('Authorization', auth).query({ email: 'usuario3@email.com' })
    )
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.items).toBeInstanceOf(Array);
      expect(response.body.items).toHaveLength(1);
      expect(response.body.items[0].email).toBe('usuario3@email.com');
    })
    .catch((error) => {
      throw error;
    });
});

test('get /users/aaaa - not found', () => {
  return request(address)
    .get('/users/aaaa')
    .set('Authorization', auth)
    .then((response) => {
      expect(response.status).toBe(404);
    })
    .catch((error) => {
      throw error;
    });
});

test('get /users/:id', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      name: 'usuario 6',
      githubUsername: 'somegithubuser',
      email: 'user6@gmail.com',
      password: '123456',
    })
    .then((response) => request(address).get(`/users/${response.body._id}`).set('Authorization', auth))
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.name).toBe('usuario 6');
      expect(response.body.githubUsername).toBe('somegithubuser');
      expect(response.body.email).toBe('user6@gmail.com');
      expect(response.body.password).toBeUndefined();
    })
    .catch((error) => {
      throw error;
    });
});

test('patch /users/aaaaa - not found', () => {
  return request(address)
    .patch(`/users/aaaaa`)
    .set('Authorization', auth)
    .then((response) => {
      expect(response.status).toBe(404);
    })
    .catch((error) => {
      throw error;
    });
});

test('patch /users/:id', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      name: 'Some Hero',
      githubUsername: 'somegithubuser',
      email: 'noreply@marvel.com',
      password: '123456', //minimum 6
    })
    .then((response) =>
      request(address)
        .patch(`/users/${response.body._id}`)
        .set('Authorization', auth)
        .send({
          name: 'Patched Hero',
        })
        .then((resp) => {
          expect(resp.status).toBe(200);
          expect(resp.body._id).toBe(response.body._id);
          expect(resp.body.name).toBe('Patched Hero');
          expect(resp.body.email).toBe('noreply@marvel.com');
          expect(resp.body.password).toBeUndefined();
        })
    )
    .catch((error) => {
      throw error;
    });
});

test('put /users/aaaaa - not found', () => {
  return request(address)
    .put(`/users/aaaaa`)
    .set('Authorization', auth)
    .then((response) => {
      expect(response.status).toBe(404);
    })
    .catch((error) => {
      throw error;
    });
});

test('put /users/:id', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      name: 'usuario 7',
      githubUsername: 'somegithubuser',
      email: 'user7@gmail.com',
      password: '123456',
      gender: 'Male',
    })
    .then((response) =>
      request(address).put(`/users/${response.body._id}`).set('Authorization', auth).send({
        name: 'usuario 7',
        githubUsername: 'somegithubuser',
        email: 'user7@gmail.com',
        password: '123456',
      })
    )
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.name).toBe('usuario 7');
      expect(response.body.githubUsername).toBe('somegithubuser');
      expect(response.body.email).toBe('user7@gmail.com');
      expect(response.body.gender).toBeUndefined();
      expect(response.body.password).toBeUndefined();
    })
    .catch((error) => {
      throw error;
    });
});

test('authenticate user - not authorized', () => {
  return request(address)
    .post('/users/authenticate')
    .send({
      email: 'admin@email.com',
      password: '123456',
    })
    .then((response) => {
      expect(response.status).toBe(403);
    })
    .catch((error) => {
      throw error;
    });
});

test('authenticate user', () => {
  return request(address)
    .post('/users/authenticate')
    .send({
      email: 'admin@adm.filippemafra.dev',
      password: '123456',
    })
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.accessToken).toBeDefined();
    })
    .catch((error) => {
      throw error;
    });
});

test('delete /users/:id', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      name: 'Some Hero Deleted',
      githubUsername: 'somegithubuser',
      email: 'noreply-deleted-t@marvel.com',
      password: '123456', //minimum 6
    })
    .then((response) =>
      request(address)
        .del(`/users/${response.body._id}`)
        .set('Authorization', auth)
        .then((resp) => {
          expect(resp.status).toBe(204);
        })
    )
    .catch((error) => {
      throw error;
    });
});

test('delete /users/:id - Already deleted', () => {
  return request(address)
    .post('/users')
    .set('Authorization', auth)
    .send({
      name: 'Some Hero Deleted Twice',
      githubUsername: 'somegithubuser',
      email: 'noreply-deleted-t@marvel.com',
      password: '123456', //minimum 6
    })
    .then((response) =>
      request(address)
        .del(`/users/${response.body._id}`)
        .set('Authorization', auth)
        .then((resp) =>
          request(address)
            .del(`/users/${resp.body._id}`)
            .set('Authorization', auth)
            .then((res) => {
              expect(res.status).toBe(404);
            })
        )
    )
    .catch((error) => {
      throw error;
    });
});
