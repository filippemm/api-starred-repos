module.exports = {
  globals: {
    address: 'http://localhost:3001',
    auth: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbkBhZG0uZmlsaXBwZW1hZnJhLmRldiIsImlzcyI6ImVpLXNhIiwiaWF0IjoxNjI1ODI2OTkyfQ.Qg3o8vGfnG0uEmN8kalz58aoAxwQDgpM7G4agsj_q6I',
  },
  testEnvironment: 'node',
  preset: 'ts-jest',
};
