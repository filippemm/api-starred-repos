import * as jestCli from 'jest-cli';

import { Server } from './server/server';
import { environment } from './common/environment';
import { usersRouter } from './entities/users/users.router';
import { User } from './entities/users/users.model';
import { repositoriesRouter } from './entities/repositories/repositories.router';
let server: Server;

const beforeAllTests = () => {
  environment.db.url = process.env.DB_URL || 'mongodb://localhost/api-test-db';
  environment.server.port = process.env.SERVER_PORT || 3001;
  server = new Server();
  return server
    .bootstrap([usersRouter, repositoriesRouter])
    .then(() => User.remove({}).exec())
    .then(() => {
      let admin = new User();
      admin.name = 'Administrador';
      admin.githubUsername = 'somegithub';
      admin.email = 'admin@adm.filippemafra.dev';
      admin.password = '123456';
      admin.profiles = ['admin', 'user'];
      return admin.save();
    });
};

const afterAllTests = () => {
  return server.shutdown();
};

beforeAllTests()
  .then(() => jestCli.run())
  .then(() => afterAllTests())
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
