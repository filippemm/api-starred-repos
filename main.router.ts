import * as restify from 'restify';
import * as packageJson from './package.json';

import { Router } from './common/router';

class MainRouter extends Router {
  applyRoutes(application: restify.Server) {
    application.get('/', (req, resp, next) => {
      resp.json({
        program: packageJson.name,
        version: packageJson.version,
      });
    });
  }
}

export const mainRouter = new MainRouter();
