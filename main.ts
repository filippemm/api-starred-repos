//Seção 3, Aula 13, instalação do começo de um novo projeto

import { Server } from './server/server';

import { usersRouter } from './entities/users/users.router';
import { mainRouter } from './main.router';
import { repositoriesRouter } from './entities/repositories/repositories.router';

const server = new Server();

server
  .bootstrap([mainRouter, usersRouter, repositoriesRouter])
  .then((server) => {
    console.log('Server is listening on: ', server.application.address());
  })
  .catch((error) => {
    console.log('Server failed to start');
    console.error(error);
    process.exit(1);
  });
