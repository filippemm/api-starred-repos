import * as restify from 'restify';
import * as jwt from 'jsonwebtoken';

import { environment } from '../common/environment';
import { NotAuthorizedError } from 'restify-errors';
import { User } from '../entities/users/users.model';

export const authenticate: restify.RequestHandler = (req, res, next) => {
  const { email, password } = req.body;
  User.findByEmail(email, '+password')
    .then((user) => {
      if (user && user.matches(password)) {
        const token = jwt.sign(
          {
            sub: user.email,
            iss: 'ei-sa',
            //exp: Math.floor(Date.now() / 1000) + (60 * 60) //-> One hour
            exp: Math.floor(Date.now() / 1000) + 4 * 60 * 60, //-> Four hours
          },
          environment.security.apiSecret
        );
        res.json({
          name: user.name,
          email: user.email,
          githubUsername: user.githubUsername,
          accessToken: token,
          profiles: user.profiles,
        });
        return next(false);
      } else {
        return next(new NotAuthorizedError('Invalid Credentials'));
      }
    })
    .catch(next);
};
