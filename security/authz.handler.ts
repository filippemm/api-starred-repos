import { Next, Request, RequestHandler, Response } from 'restify';

import { ForbiddenError, UnauthorizedError } from 'restify-errors';

export const authorize: (...profiles: string[]) => RequestHandler = (...profiles) => {
  return (req: Request, res: Response, next: Next) => {
    if (req.authenticated !== undefined && req.authenticated.hasAny(...profiles)) {
      next();
      req.log.debug(
        'User %s is authorized with profiles %j on route %s. Required profiles %j with method %s',
        req.authenticated._id,
        req.authenticated.profiles,
        req.path(),
        profiles,
        req.getRoute().method
      );
    } else {
      if (req.authenticated) {
        req.log.debug(
          'Permission denied for %s. Required profiles: %j. User profiles: %j',
          req.authenticated._id,
          profiles,
          req.authenticated.profiles
        );
        next(new ForbiddenError('Permission denied'));
      } else {
        next(new UnauthorizedError('Permission denied'));
      }
    }
  };
};
