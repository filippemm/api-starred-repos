import * as jwt from 'jsonwebtoken';
import { Request, RequestHandler } from 'restify';

import { environment } from '../common/environment';
import { NotAuthorizedError } from 'restify-errors';
import { User } from '../entities/users/users.model';

export const tokenParser: RequestHandler = (req, res, next) => {
  const token = extractToken(req);
  if (token) {
    jwt.verify(token, environment.security.apiSecret, applyBearer(req, next));
  } else {
    next();
  }
};

function extractToken(req: Request) {
  let token = undefined;
  const authorization = req.header('authorization');
  if (authorization) {
    const parts: string[] = authorization.split(' ');
    if (parts.length === 2 && parts[0] === 'Bearer') {
      token = parts[1];
    }
  }
  return token;
}

function applyBearer(req: Request, next): (error, decoded) => void {
  return (error, decoded) => {
    if (decoded) {
      User.findByEmail(decoded.sub)
        .then((user) => {
          if (user) {
            req.authenticated = user;
          }
          next();
        })
        .catch(next);
    } else {
      next();
    }
  };
}
