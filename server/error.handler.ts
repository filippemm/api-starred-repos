import { Request, Response } from 'restify';
// import { cloneDeep } from 'lodash';

export const handleError = (req: Request, res: Response, err, done) => {
  // TODO check for a better way to do this
  // mongoose error do not allow overwrite toJSON
  const errName = err.name;
  if (err.name === 'ValidationError') {
    // err = cloneDeep(err);
    err.statusCode = 400;
    const messages: any[] = [];
    for (let name in err.errors) {
      messages.push({ message: err.errors[name].message });
    }
    err = Object.assign({}, err);
    err.toJSON = () => ({
      message: 'Validation error while processing your request',
      errors: messages,
    });
  } else {
    err.toJSON = () => {
      return {
        message: err.message,
      };
    };
  }

  if (err.code === 66832) {
    console.log(`Unexpected error ${err.name}`);
  }

  switch (err.name) {
    case 'MongoError': //CastError
      if (err.code === 11000) {
        err.statusCode = 400;
      }
      break;
    case 'ValidationError':
      err.statusCode = 400;
      const messages: any[] = [];
      for (let name in err.errors) {
        messages.push({ message: err.errors[name].message });
      }
      err.toJSON = () => ({
        message: 'Validation error while processing your request',
        errors: messages,
      });
      break;
    default:
      break;
  }
  done();
};
