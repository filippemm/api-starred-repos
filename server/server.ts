import * as restify from 'restify';
import * as mongoose from 'mongoose';
import * as fs from 'fs';
import * as corsMiddleware from 'restify-cors-middleware';
import * as packageJson from '../package.json';

import { environment } from '../common/environment';
import { Router } from '../common/router';
import { logger } from '../common/logger';
import { mergePatchBodyParser } from './merge-patch.parser';
import { tokenParser } from '../security/token.parser';
import { handleError } from './error.handler';

export class Server {
  application: restify.Server;

  initializeDb() {
    return mongoose.connect(environment.db.url);
  }

  initRoutes(routers: Router[]): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const options: restify.ServerOptions = {
          name: packageJson.name,
          version: packageJson.version,
          log: logger,
        };

        if (environment.security.enableHTTPS) {
          options.certificate = fs.readFileSync(environment.security.certificate);
          options.key = fs.readFileSync(environment.security.key);
        }

        this.application = restify.createServer(options);

        // TODO change the CORS option to restrict other urls
        const corsOptions: corsMiddleware.Options = {
          preflightMaxAge: 10,
          origins: ['*'],
          allowHeaders: ['authorization'],
          exposeHeaders: ['x-custom-header'],
        };
        const cors: corsMiddleware.CorsMiddleware = corsMiddleware(corsOptions);

        this.application.pre(cors.preflight);

        this.application.pre(
          restify.plugins.requestLogger({
            log: logger,
          })
        );

        this.application.use(cors.actual);
        this.application.use(restify.plugins.queryParser());
        this.application.use(restify.plugins.bodyParser());
        this.application.use(mergePatchBodyParser);
        this.application.use(tokenParser);

        // ---> Routes
        for (const router of routers) {
          router.applyRoutes(this.application);
        }
        // <--- Routes

        this.application.listen(environment.server.port, () => {
          resolve(this.application);
        });

        this.application.on('restifyError', handleError);
      } catch (error) {
        reject(error);
      }
    });
  }

  async bootstrap(routers: Router[] = []): Promise<Server> {
    return this.initializeDb().then(() => this.initRoutes(routers).then(() => this));
  }

  async shutdown(): Promise<void> {
    return mongoose.disconnect().then(() => {
      this.application.close();
    });
  }
}
