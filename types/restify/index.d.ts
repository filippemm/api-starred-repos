import { User } from '../../entities/users/users.model';

declare module 'restify' {
  interface Request {
    authenticated: User;
  }
}
